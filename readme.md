**Wordpress Stack Docker Compose Configuration**

This configuration consists the following services:
- Wordpress
- PHP-FPM
- Nginx
- Mysql
- Phpmyadmin

All the services, when build, will produce the latest version of the respective service.

Follow the below steps to setup your own wordpress stack:
1. Copy/rename ".env.example" to ".env"
2. Fill in the desired values in the ".env" file
3. Navigate to docker-compose.yml in your terminal
4. Run: docker-compose up -d

Other Commands:
1. View all the running containers:
docker-compose ps   OR   docker ps
2. Stop all the containers:
docker-compose down
